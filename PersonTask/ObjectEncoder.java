import java.util.*;
import java.io.*;

public class ObjectEncoder{
	public static void main(String[] args){
		ArrayList<Person> persons = new ArrayList<Person>();
		Person p1 = new Person(1, "Niha" , 5);
		Person p2 = new Person(2, "Jumana", 6);
		Person p3 = new Person(3, "Humaira", 7);
		
		persons.add(p1);
		persons.add(p2);
		persons.add(p3);
		
		try{
			FileWriter fw = new FileWriter("Person.txt");
			
			for (Person p : persons){
				System.out.print(p.getId());
				System.out.print(",");
				System.out.print(p.getName());
				System.out.print(",");
				System.out.print(p.getAge());
				System.out.print("\n");
				
				fw.write(p.getId() + "," + p.getName() + "," +p.getAge() + "\n");
			}
			fw.close();
		}catch (Exception e){
			System.out.println(e);
		}
	}
}
