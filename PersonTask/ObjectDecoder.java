import java.io.*;
import java.util.*;
import java.io.BufferedReader;

public class ObjectDecoder{
    public static void main(String[] args){
        String str1 = null;

        ArrayList<Person> persons = new ArrayList<Person>();

        if(args.length<1){
            System.out.println("Please pass two arguments");
            System.exit(0);
        }

        str1 = args[0];

        try{
            File src_file = new File(str1);

            if(src_file.isDirectory()){
                System.out.println("The path is a directory");
                System.exit(0);
            }

            FileReader fr = new FileReader(src_file);
            BufferedReader br = new BufferedReader(fr);
            String line;

            try{
                while((line = br.readLine()) != null){
                    String[] arrOfStr = line.split(",",3);

                    persons.add(new Person(Long.parseLong(arrOfStr[0]), arrOfStr[1], Integer.parseInt(arrOfStr[2])));
                }
                br.close();
                fr.close();
            }catch(IOException e){
                System.out.println(e.getMessage());
            }
        }catch(FileNotFoundException e){
            System.out.println(e.getMessage());
        }

        for(Person person:persons){
            System.out.println(person.toString());
        }
    }
}
